<?php
namespace Endeavour\Metatable;

use Endeavour\Base;

abstract class Metatable extends Base {
	public function __construct(string $table_name, array $key_fields) {
		parent::__construct($table_name, $key_fields);
	}
	
	protected function key_fields_str(): string {
		return implode(", ", $this->key_field);
	}
	
	protected function get_key_fields_data(): array {
		return array_intersect_key($this->data, array_flip($this->key_field));
	}
	
	protected function get_key_fields_values(): array {
		return array_values($this->get_key_fields_data());
	}
	
	protected function prepare_keys(): string {
		$key_fields = [];
		foreach ($this->key_field as $index => $value) {
			$key_fields[$value] = $value . "= :" . $value;
		}
		
		return implode(" AND ", $key_fields);
	}
	
	protected function is_new(string ...$ids): bool {
		$key_fields = $this->prepare_keys();

		$sql = "SELECT {$this->key_field[0]}
				FROM $this->table_name
				WHERE $key_fields";

		$is_new = $this->db->fetchColumn($sql, array_combine($this->key_field, $ids));

		return $is_new === false;
	}
	
	//TODO Add in functionality for UUID
	public function save(): bool {
		$is_new = $this->is_new(...$this->get_key_fields_values());

		if ($is_new) {
			$is_saved = $this->db->insert($this->table_name, $this->data);
		} else {
			//TODO Remove key_fields from set
			$is_saved = $this->db->update($this->table_name, $this->data, $this->get_key_fields_data());
		}
		
		return $is_saved > 0;
	}
	
	public function load(string ...$ids): bool {
		$fields = implode(", ", $this->get_column_names());
		$key_fields = $this->prepare_keys();
		
		$sql = "SELECT $fields
				FROM $this->table_name
				WHERE $key_fields";
		
		$data = $this->db->fetchAssoc($sql, array_combine($this->key_field, $ids));
		if ($data !== false) {
			$this->data = $data;
		}
		
		return $data !== false;
	}
	
	public function delete(string ...$ids): bool {
		$is_deleted = $this->db->delete($this->table_name, array_combine($this->key_field, $ids));
		
		return $is_deleted > 0;
	}
}
?>