<?php
namespace Endeavour\Auth;

use Endeavour\Metatable\Metatable;
use Ramsey\Uuid\Uuid;

class Auth extends Metatable {
	//TODO Make this a config
	private const PEPPER = "o8vpHCjwzeyFnxxA5uRw4UYrkjRJkPRX";
	
	public $admin_mode = "admin";

	public function __construct(bool $admin_mode = false, string $table_name = "user", array $key_fields = ["user_id"]) {
		parent::__construct($table_name, $key_fields);
		
		$this->admin_mode = $admin_mode;
	}
	
	protected function hash_password(string $password): string {
		return password_hash(Auth::PEPPER . $password, PASSWORD_DEFAULT);
	}
	
	public function validate(array $values): bool {
		$username_max_length = 50;
		$password_min_length = 8;
		
		$username_is_valid = true;
		$password_is_valid = true;
		
		$this->set($values);
		
		if (!empty($this->data["username"])) {
			$username_is_valid = strlen($this->data["username"]) <= $username_max_length;
			
			if (!$username_is_valid) {
				$this->add_error("form", "Username must be no longer than $username_max_length characters.");
			}
		} else {
			$this->add_error("form", "Username is required");
			
			$username_is_valid = false;
		}
		
		if (!empty($this->data["password"])) {
			$password_is_valid = (bool)preg_match("/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{" . $password_min_length . ",})\S$/", $this->data["password"]);
			
			if (!$password_is_valid) {
				$this->add_error("form", "Password must contain at least $password_min_length characters with at least 1 uppercase letter, 1 lowercase letter, 1 number, and must not contain spaces");
			}
		} else if (!$this->admin_mode) {
			$password_is_valid = false;
			
			$this->add_error("form", "Password is required.");
		}
		
		if (!empty($this->data["password"])) {
			if ($password_is_valid) {
				$this->data["password"] = $this->hash_password($this->data["password"]);
			}
		} else {
			unset($this->data["password"]);
		}
		
		if (!empty($this->data["permissions"])) {
			$this->data["permissions"] = json_encode($this->data["permissions"]);
		}
		
		return $password_is_valid && $username_is_valid;
	}
	
	public function load(string ...$id): bool {
		$is_loaded = parent::load($id[0]);
		
		if (!empty($this->data["permissions"])) {
			$this->data["permissions"] = json_decode($this->data["permissions"]);
		}
		
		return $is_loaded;
	}
	
	public function save(): bool {
		if ($this->is_new()) {
			$this->data[$this->key_field] = Uuid::uuid4();
		}
		
		return parent::save();
	}
	
	public function login(string $username, string $password): bool {
		$fields = implode(", ", $this->get_column_names());
		$key_fields = $this->key_fields_str();
		
		$sql = "SELECT $fields
				FROM $this->table_name
				WHERE username = :username";
		
		$this->data = $this->db->fetchAssoc($sql, ["username" => $username]);
		
		if (password_verify(Auth::PEPPER . $password, $this->data["password"])) {
			$_SESSION["user"] = [
				$key_fields		=> $this->data[$key_fields],
				"username"		=> $this->data["username"],
				"permissions"	=> json_decode($this->data["permissions"])
			];

			return true;
		}

		return false;
	}
	
	public function page_access(string $permission): void {
		if ($permission != "guest") {
			if (!in_array($permission, $_SESSION["user"]["permissions"])) {
				header("Location: index.php");
			}
		}
	}

	public function logout(): void {
		unset($_SESSION["user"]);
		session_regenerate_id(true);
		
		header("Location: index.php");
		exit;
	}
	
	/**
	 * @return \Doctrine\DBAL\Driver\Statement|int
	 */
	public function get_users() {
		$query = $this->db->createQueryBuilder()
			->select($this->get_column_names())
			->from($this->table_name);
		
		return $query->execute();
	}
	
	/**
	 * @return \Doctrine\DBAL\Driver\Statement|int
	 */
	public function get_permission_list() {
		$query = $this->db->createQueryBuilder()
			->select(["name", "descript"])
			->from("auth_permissions")
			->orderBy("seq_no", "ASC");
		
		return $query->execute();
	}
}
?>