<?php
namespace Endeavour\Database;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;

class Database {
	/**
	 * @param string $database
	 * @param string $username
	 * @param string $password
	 * @param string $hostname
	 * @param string $port
	 * @param string $charset
	 * @param string $driver
	 *
	 * @return \Doctrine\DBAL\Connection|false
	 */
	static public function create_db(string $database, string $username, string $password, string $hostname = "localhost", string $port = "3306", string $charset = "utf8", string $driver = "mysql") {
		$url = "$driver://$username:$password@$hostname:$port/$database?charset=$charset";
		
		try {
			return DriverManager::getConnection(["url" => $url]);
		} catch (DBALException $exception) {
			return false;
		}
	}
	
	/**
	 * @return \Doctrine\DBAL\Connection|false
	 */
	static public function init_db() {
		$database 	= DB_NAME;
		$username 	= DB_USER;
		$password 	= DB_PASS;
		$hostname 	= DB_HOST;
		$port		= DB_PORT;
		$charset	= DB_CHARSET;
		$driver		= DB_DRIVER;
		
		return self::create_db($database, $username, $password, $hostname, $port, $charset, $driver);
	}
}
?>