<?php
namespace Endeavour;

use Doctrine\DBAL\Schema\Table;
use Endeavour\Database\Database;

//TODO Remove DB
abstract class Base {
	protected $table_name;
	protected $key_field;
	protected $db;
	protected $schema;
	
	protected $data = [];
	protected $errors = [];
	
	public function __construct(string $table_name, $key_field) {
		$this->table_name = $table_name;
		$this->key_field = $key_field;
		
		$this->db = Database::init_db();
		$this->schema = $this->init_schema();
	}
	
	/**
	 * @return \Doctrine\DBAL\Schema\Table
	 */
	protected function init_schema(): Table {
		return $this->schema = $this->db->getSchemaManager()->listTableDetails($this->table_name);
	}
	
	protected function get_column_names(): array {
		return array_keys($this->schema->getColumns());
	}
	
	abstract protected function is_new(string ...$id): bool;
	
	public function add_error(string $index, string $error): void {
		$this->errors[$index][] = $error;
	}

	public function get_errors(): array {
		return $this->errors;
	}
	
	public function sanitize(array $data): array {
		return filter_var_array($data, FILTER_SANITIZE_STRING);
	}
	
	public function set(array $data): void {
		$this->data = $this->sanitize($data);
	}
	
	abstract public function validate(array $values): bool;
	
	abstract public function save(): bool;
	
	abstract public function load(string ...$id): bool;
	
	abstract public function delete(string ...$id): bool;
	
	public function get(): array {
		return $this->data;
	}
}
?>