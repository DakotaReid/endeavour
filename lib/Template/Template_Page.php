<?php
namespace Endeavour\Template;

class Template_Page extends Template {
	public function __construct(string $tpl, array $parameters) {
		parent::__construct($tpl);
		
		$this->vars = $parameters;
	}
	
	public function fetch_template(): string {
		$base_template = new Template("template");
		$base_template->set_multi($this->vars);
		
		$is_logged_in = !empty($_SESSION["user"]) ? true : false;
		$is_admin = false;
		
		if (!empty($_SESSION["user"]["permissions"])) {
			$is_admin = in_array("admin", $_SESSION["user"]["permissions"]);
		}
		
		$header_template = new Template("header");
		$header_template->set_multi($this->vars);
		$header_template->set("is_logged_in", $is_logged_in);
		$header_template->set("is_admin", $is_admin);
		
		$content_template = new Template($this->tpl);
		$content_template->set_multi($this->vars);
		$content_template->set("is_logged_in", $is_logged_in);
		$content_template->set("is_admin", $is_admin);
		
		$footer_template = new Template("footer");
		$footer_template->set_multi($this->vars);
		
		$base_template->set_multi([
			"header"	=> $header_template->fetch_template(),
			"content"	=> $content_template->fetch_template(),
			"footer"	=> $footer_template->fetch_template()
		]);
		
		return $base_template->fetch_template();
	}
}
?>