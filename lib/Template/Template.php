<?php
namespace Endeavour\Template;

class Template {
	protected $tpl;
	protected $vars = [];
	
	public function __construct(string $tpl) {
		$this->tpl = $tpl;
	}
	
	public function set(string $name, $value) : void {
		$this->vars[$name] = $value;
	}
	
	public function set_multi(array $values) : void {
		foreach ($values as $name => $value) {
			$this->set($name, $value);
		}
	}
	
	public function fetch_template() : string {
		extract($this->vars);
		
		ob_start();
		
		include stream_resolve_include_path("$this->tpl.tpl.php");
		$contents = ob_get_contents();
		
		ob_clean();
		
		return $contents;
	}
}
?>